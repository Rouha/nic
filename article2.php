<!DOCTYPE html>
<html>
<head>
    <title>Jan Rouha | Grombel</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="styles/dist/compiled_styles.min.css">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="shortcut icon" type="image/png" href=""/>
</head>
<body>


<div id="Article">

    <p id="Demo"></p>

</div>


<script src="js/jquery-ui.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/main2.js"></script>

</body>
</html>