<!DOCTYPE html>
<html>
<head>
    <title>Jan Rouha | Grombel</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="styles/dist/compiled_styles.min.css">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script type = "text/javascript" src = "js/jquery213.min.js"></script>

    <script type = "text/javascript" src = "js/jquery-ui1113.min.js"></script>

    <script type = "text/javascript" src = "js/textEfftect1.js"></script>

    <script type = "text/javascript" src = "js/main3.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href=""/>

</head>
<body>

<div id="Slovo6">

<div class="container">

    <div class="content">
        <svg viewBox="0 0 1024 768">

            <!-- Symbol -->
            <symbol id="s-text">
                <text text-anchor="middle" x="50%" y="50%" dy=".35em">
                    slovo
                </text>
            </symbol>

            <!-- Duplicate symbols -->
            <use xlink:href="#s-text" class="text"></use>
            <use xlink:href="#s-text" class="text"></use>
            <use xlink:href="#s-text" class="text"></use>
            <use xlink:href="#s-text" class="text"></use>
            <use xlink:href="#s-text" class="text"></use>

        </svg>
    </div>

</div>

</div>



</body>
</html>