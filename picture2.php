<!DOCTYPE html>
<html>
<head>
    <title>Picture</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Ultrasonic picture">
    <meta name="keywords" content="ultrasonic sensor, distance, picture">
    <meta name="author" content="Jan Rouha">

    <link rel="stylesheet" href="styles/dist/compiled_styles.min.css">

    <script type = "text/javascript" src = "js/jquery.min.js"></script>
    <script type = "text/javascript" src = "js/jquery-ui.js"></script>
    <script type = "text/javascript" src = "js/number.js"></script>
    <script type = "text/javascript" src = "js/picture-slider.js"></script>

    <link rel="shortcut icon" type="image/png" href=""/>

</head>
<body>

    <div id="PictureBody">

        <input id="Slider" type="range" value="400" min="0" max="400">

        <div id="StartBox">


        </div>

        <div id="DistanceValue">400</div>


    </div>

</body>
</html>