<!DOCTYPE html>
<html>
<head>
    <title>Jan Rouha | Grombel</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="styles/dist/compiled_styles.min.css">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script type = "text/javascript" src = "js/jquery213.min.js"></script>

    <script type = "text/javascript" src = "js/jquery-ui1113.min.js"></script>

    <script type = "text/javascript" src = "js/textEfftect1.js"></script>

    <script type = "text/javascript" src = "js/main3.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href=""/>

</head>
<body>


<div id="Slovo8">

    <h1 class="stroke">
        <svg xmlns="http://www.w3.org/2000/svg" width="640" height="250" viewBox="0 0 640 250">
            <text x="0" y="225" class="svgStroke">slovo</text>
        </svg>
    </h1>

</div>

</body>
</html>