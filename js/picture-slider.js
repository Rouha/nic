$(document).ready(function() {

    // DOM PREPARE

    var windowWidth = $(document).width();
    var windowHeight = $(document).height();

    var windowCenterX = windowWidth / 2;
    var windowCenterY = windowHeight / 2;

    var startingPhaseCalled = false;

    var startBox = $("#StartBox");

    $(startBox).css("width", windowWidth / 4);
    $(startBox).css("height", windowHeight / 2);
    $(startBox).css( { marginLeft : "auto", marginRight : "auto", marginBottom : "0px", marginTop: windowHeight - startBox.height() - windowHeight / 8} );

    // CONTENT PREPARE

    var source_text = "Žádám vás snažně, abyste nepodpořili ty, kteří vám slibují, že všechno vyřeší za vás. Takoví lidé chtějí, abyste jen mlčeli, poslouchali a drželi krok. Žádám vás snažně, abyste nepodpořili ty, kteří mají diktátorské sklony, příliš často mění názory, nejsou schopni se domluvit s jinými, nabízejí různá dobrodružná, nepromyšlená a neodpovědná řešení a kteří by se nejraději vrátili k centralistickému řízení všech našich společných věcí. Žádám vás snažně, abyste nepodpořili ty, kteří vám slibují, že všechno vyřeší za vás. Takoví lidé chtějí, abyste jen mlčeli, poslouchali a drželi krok. ";

    var goodWords = ["snažně ", "snažně, ", "Žádám ", "nabízejí ", "společných ", "řešení "];
    var badWords = ["chtějí ", "chtějí, ", "mlčeli ", "krok ", "diktátorské ", "řízení "];

    var words = source_text.split(" ");
    var wordsInfo = [];

    var randomOperator = 0;
    var randomCoefficient = 1;
    var randomCoefficient2 = 1;
    var randomCoefficient3 = 5;


    setInterval(function () {

        randomOperator = Math.floor(Math.random() * 9);
        randomCoefficient = Math.floor(Math.random() * 3) + 1;
        randomCoefficient2 = Math.floor(Math.random() * 6) + 1;
        randomCoefficient3 = Math.floor(Math.random() * 10) + 3;

    }, 5000);

    for (var i = 0; i < words.length; i++) {

        $(startBox).append('<span class="startPhase word word_' + i + '">' + (words[i] += " ") +'</span>').show();

        var span = $(".word_" + i);
        var offset = $(span).offset();
        var elementWidth = $(span).width();
        var elementHeight = $(span).height();

        var rangeLen = Math.floor(Math.random() * 75);
        var start = Math.floor(Math.random() * (400 - rangeLen));
        var end = Math.floor(start + rangeLen);

        //var range = Math.floor(Math.random() * 300);

        wordsInfo.push({
            number: i,
            content: words[i],
            startTop: offset.top,
            startLeft:  offset.left,
            currentTop: offset.top,
            currentLeft: offset.left,
            angle: 0,
            fontSize: 20,
            color: 0,
            start: start,
            end: end,
            status: 0,
            importance: null,
            operatorX: null,
            operatorY: null,
            width: elementWidth,
            height: elementHeight,
            type: 0,
            stepX: 0,
            stepY: 0,
            diffXn: 0,
            diffYn: 0,
            diffXn2: 0,
            diffYn2: 0
        });

        //SET GOOD AND BAD WORDS
        if (jQuery.inArray(wordsInfo[i].content, goodWords)!='-1') {
            wordsInfo[i].importance = 1;
        } else if (jQuery.inArray(wordsInfo[i].content, badWords)!='-1'){
            wordsInfo[i].importance = 2;
        }

        //SET DEFAULT OPERATORS
        if (wordsInfo[i].startLeft <= windowWidth / 2){
            wordsInfo[i].operatorX = "-=";
        }else {
            wordsInfo[i].operatorX = "+=";
        }

        if (wordsInfo[i].startTop >= windowHeight / 2){
            wordsInfo[i].operatorY = "+=";
        }else {
            wordsInfo[i].operatorY = "-=";
        }

        //set step values
        var diffX = Math.abs(windowCenterX - wordsInfo[i].startLeft);
        var diffY = Math.abs(windowCenterY - wordsInfo[i].startTop);
        wordsInfo[i].diffXn = windowCenterX - diffX;
        wordsInfo[i].diffYn = windowCenterY - diffY;
        wordsInfo[i].diffXn2 = ((diffX / diffY) * wordsInfo[i].diffYn);
        wordsInfo[i].diffYn2 = ((diffY / diffX) * wordsInfo[i].diffXn);

        if (wordsInfo[i].diffXn2 > wordsInfo[i].diffXn) {
            wordsInfo[i].stepX = wordsInfo[i].diffXn / 400;
            wordsInfo[i].stepY = ((diffY / diffX) * wordsInfo[i].diffXn) / 400;
        } else {
            wordsInfo[i].stepX = ((diffX / diffY) * wordsInfo[i].diffYn) / 400;
            wordsInfo[i].stepY = wordsInfo[i].diffYn / 400;
        }

        if (wordsInfo[i].importance != null) {

            $(".word_" + i).addClass("zIndex");

        }




    }

    console.log(wordsInfo);

    //CREATING SVG

    //$(".word_1").remove();

    absoluteMassacre();

    function absoluteMassacre(){

        for (var i = 0; i < wordsInfo.length; i++) {

            $(".word_" + wordsInfo[i].number).css({ top: wordsInfo[i].startTop, left: wordsInfo[i].startLeft, position: "absolute" }).removeClass("startPhase");

        }

    }




    /*
    var socket = io();

    var floatingFilterArray = [];

    var filteredDistance = 400;

    socket.on('distance', function(data) { //get light switch status from client
        if (data) {
            document.getElementById("Distance").innerHTML = data;
            console.log(data);

            if (data < 400 && Math.round(data) != 0)
                floatingFilterArray.push(Math.round(data));

            if (floatingFilterArray.length > 60) {
                var floatingFilterArrayCopy = Array.from(floatingFilterArray).sort(); // pracovni kopie pole

                filteredDistance = floatingFilterArrayCopy[30];

                floatingFilterArray.shift();
            }
        }
    });

    setInterval(function() {
        document.getElementById("DistanceValue").innerHTML = filteredDistance;
    }, 34);
     */

    $("#Slider").on('input', function() { $("#DistanceValue").html(this.value); });




    //INTERVAL, KDE SE BUDOU AKTIVOVAT ŠPATNÁ A DOBRÁ SLOVA PRO AKTUÁLNÍ POUŽITÍ A U NEAKTIVNÍCH SE BUDE MĚNIT RANGE

    //MAIN INTERVAL

    var lastDistanceValue = $("#DistanceValue").text();

    var pictureBody = $("#PictureBody");

    setInterval(function () {

        var currentDistance = 400 - $("#DistanceValue").text();
        var diffenceDistance = lastDistanceValue - currentDistance;
        lastDistanceValue = currentDistance;
        //console.log(diffenceDistance);
        //console.log(currentDistance);

        if (lastDistanceValue <= 0) {
            if (startingPhaseCalled === false) {
                startingPhase();
            }
        } else {
            startingPhaseCalled = false;
            for (var i = 0; i < wordsInfo.length; i++){

                if (wordsInfo[i].importance == null){

                   /* if (wordsInfo[i].currentLeft <= 0 || (wordsInfo[i].currentLeft + wordsInfo[i].width) >= windowWidth){


                    } else {*/

                        if (wordsInfo[i].operatorX == "+="){

                            wordsInfo[i].currentLeft = wordsInfo[i].startLeft + currentDistance * ((wordsInfo[i].stepX * 400) - wordsInfo[i].width) / 400;

                            $(".word_" + i).css({
                                "left" : wordsInfo[i].currentLeft + "px"
                            });

                        } else if (wordsInfo[i].operatorX == "-="){

                            wordsInfo[i].currentLeft = wordsInfo[i].startLeft - currentDistance * wordsInfo[i].stepX;

                            $(".word_" + i).css({
                                "left" : wordsInfo[i].currentLeft + "px"
                            });

                        }


                    //}

                    /*if (wordsInfo[i].currentTop <= 0 || (wordsInfo[i].currentTop + wordsInfo[i].height) >= windowHeight){

                    } else {*/

                        if (wordsInfo[i].operatorY == "+="){

                            wordsInfo[i].currentTop = wordsInfo[i].startTop + currentDistance * ((wordsInfo[i].stepY * 400) - wordsInfo[i].height) / 400;

                            $(".word_" + i).css({
                                "top" : wordsInfo[i].currentTop + "px"
                            });

                        } else if (wordsInfo[i].operatorY == "-="){

                            wordsInfo[i].currentTop = wordsInfo[i].startTop - currentDistance * wordsInfo[i].stepY;

                            $(".word_" + i).css({
                                "top" : wordsInfo[i].currentTop + "px"
                            });

                        }

                    //}

                }

                if (wordsInfo[i].importance != null) {
                    if ((wordsInfo[i].start < currentDistance)){
                        if ((wordsInfo[i].end > currentDistance)) {

                            $(".word_" + i).removeClass("rotateCheat");

                            //Color
                            wordsInfo[i].color = Math.round(255 / 400) * currentDistance;

                            if (randomOperator <= 4){

                                wordsInfo[i].angle -= diffenceDistance * randomCoefficient;
                                wordsInfo[i].currentTop -= diffenceDistance * randomCoefficient2;
                                wordsInfo[i].currentLeft -= diffenceDistance * randomCoefficient2;
                                wordsInfo[i].fontSize += diffenceDistance * randomCoefficient3;

                                var angle = "rotate(" + (wordsInfo[i].angle) + "deg)";
                                var fontSize = wordsInfo[i].fontSize + "px";

                                $(".word_" + i).css({
                                    "transform": angle,
                                    "color" : "rgb(" + wordsInfo[i].color + ", 0, 0)",
                                    "top" : wordsInfo[i].currentTop + "px",
                                    "left" : wordsInfo[i].currentLeft + "px",
                                    "font-size" : fontSize
                                });

                            } else {

                                wordsInfo[i].angle += diffenceDistance * randomCoefficient;
                                wordsInfo[i].currentTop += diffenceDistance * randomCoefficient;
                                wordsInfo[i].currentLeft += diffenceDistance * randomCoefficient;
                                wordsInfo[i].fontSize += diffenceDistance * randomCoefficient3;

                                var angle2 = "rotate(" + (wordsInfo[i].angle) + "deg)";
                                var fontSize2 = wordsInfo[i].fontSize + "px";

                                $(".word_" + i).css({
                                    "transform": angle2,
                                    "color" : "rgb(" + wordsInfo[i].color + ", 0, 0)",
                                    "top" : wordsInfo[i].currentTop + "px",
                                    "left" : wordsInfo[i].currentLeft + "px",
                                    "font-size" : fontSize2
                                });

                            }
                        }
                    }
                }
            }
        }

    }, 34);

    setInterval(function () {
        // nastav range pro ty co nejsou v intervalu = nejsou aktualne pouzivane
        for (var i = 0; i < words.length; i++) {
            if (wordsInfo[i].importance != null) {
                if ((wordsInfo[i].start < lastDistanceValue)){
                    if ((wordsInfo[i].end > lastDistanceValue)) {
                        var rangeLen = Math.floor(Math.random() * 75);
                        wordsInfo[i].start = Math.floor(Math.random() * (400 - rangeLen));
                        wordsInfo[i].end = Math.floor(start + rangeLen);
                        //Ukládat úhel - ale takhle asi ne
                        //wordsInfo[i].angle = parseInt($(".word_0").css('transform').split(',')[5]);



                    }
                }
            }
        }
        console.log("new generate")
    }, 5000);

    function startingPhase() {
        startingPhaseCalled = true;
        for (var i = 0; i < wordsInfo.length; i++){

            if (wordsInfo[i].importance != null) {
                var startingWord = $(".word_" + i);

                $(startingWord).addClass("rotateCheat");
                $(startingWord).animate({
                    left: wordsInfo[i].startLeft,
                    top: wordsInfo[i].startTop,
                    "font-size": "20px",
                    "color": "black"

                });

                wordsInfo[i].currentLeft = wordsInfo[i].startLeft;
                wordsInfo[i].currentTop = wordsInfo[i].startTop;
                wordsInfo[i].angle = 0;
                wordsInfo[i].fontSize = 20;
                wordsInfo[i].color = "rgb(0, 0, 0)";
            }

        }

        startingPhase2();
        console.log("RESET");

    }

    function startingPhase2() {

        for (var i = 0; i < wordsInfo.length; i++){

            if (wordsInfo[i].importance === null) {
                var startingWord = $(".word_" + i);

                wordsInfo[i].currentLeft = wordsInfo[i].startLeft;
                wordsInfo[i].currentTop = wordsInfo[i].startTop;

                $(startingWord).animate({
                    left: wordsInfo[i].startLeft,
                    top: wordsInfo[i].startTop
                });


            }

        }


    }


});