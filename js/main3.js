$(document).ready(function() {

    var slovo = $( "span.word" );

    //$( slovo ).jqIsoText({ fromSize: 50, toSize: 10 });

    smaller();

    function smaller() {

        setTimeout(bigger, 1000);

        $(slovo).animate({fontSize: 75}, "slow");
        $(slovo).addClass("bold");



    }

    function bigger() {

        setTimeout(smaller, 1000);

        $(slovo).animate({fontSize: 150}, "slow");
        $(slovo).removeClass("bold");




    }

    var slovo2 = $( "span.word2" );

    //$( slovo ).jqIsoText({ fromSize: 50, toSize: 10 });

    font1();

    function font1() {

        setTimeout(font2, 1000);

        $(slovo2).removeClass("fontChange2");
        $(slovo2).addClass("fontChange1");



    }

    function font2() {

        setTimeout(font1, 1000);

        $(slovo2).removeClass("fontChange1");
        $(slovo2).addClass("fontChange2");



    }



    var slovo4 = $( "span.word4" );

    color1();

    function color1() {

        setTimeout(color2, 1500);

        $(slovo4).removeClass("color2");
        $(slovo4).addClass("color1");



    }

    function color2() {

        setTimeout(color1, 1500);

        $(slovo4).removeClass("color1");
        $(slovo4).addClass("color2");



    }

    var slovo7 = $( "span.word7" );

    move();

    function move(){

        var topCoord = Math.floor(Math.random() * 768);
        var leftCoord = Math.floor(Math.random() * 1366);
        var duration = Math.floor(Math.random() * 2500) + 500;
        var intervalTime = Math.floor(Math.random() * 5000) + 2500;
        var fontSize = Math.floor(Math.random() * 300) + 10;

        var colorPool = Array('red', 'black', 'yellow', 'white', 'blue', 'green');
        var fontColor = colorPool[Math.floor(Math.random()*colorPool.length)];

        $(slovo7).animate({'position':'absolute', 'top':topCoord, 'left':leftCoord, 'font-size': fontSize, 'color': fontColor}, duration);
        setTimeout(move, intervalTime);

    }

    var slovo8 = $( ".svgStroke" );

    strokeSize();

    function strokeSize(){

        var intervalTime = Math.floor(Math.random() * 800) + 501;
        var duration = Math.floor(Math.random() * 500) + 200;
        var stroke = Math.floor(Math.random() * 20) + 1;

        $(slovo8).animate({'stroke-width':stroke}, duration);
        setTimeout(strokeSize, intervalTime);

    }



});