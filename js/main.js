$(document).ready(function() {

    $('input').on('blur change click dblclick error focus focusin focusout hover keydown keypress keyup load mousedown mouseenter mouseleave mousemove mouseout mouseover mouseup resize scroll select submit', function () {
        var v = $(this).val();
        var babis = 0 + v;
        var pravda = 150 - v;
        $('#babis').css('font-size', babis + 'px');
        $('#pravda').css('font-size', pravda + 'px');
    });


    var source_text = "Nový bezpečnostní poradce amerického prezidenta Donalda Trumpa John Bolton, který v dubnu nahradí odvolaného Herberta Raymonda McMastera, vyvolává především na Blízkém východě podle agentury Reuters rozporuplné reakce. Bolton je znám ostrými názory a konfrontačními postoji vůči Íránu, KLDR i Rusku, naopak je obhájcem politiky Izraele. Devětašedesátiletý Bolton v minulosti prosazoval použití vojenské síly proti Íránu či Severní Koreji a velmi ostře se stavěl například i k Rusku. V prvních dvou případech volí i Trump konfrontační politiku, k Rusku má však méně kritický přístup. Nynější Trumpovo rozhodnutí pogratulovat ruskému prezidentovi Vladimiru Putinovi ke znovuzvolení, ale označil za zdvořilostní záležitost. Trumpovi poradci přitom prezidentovi doporučili, aby Putinovi neblahopřál. Jmenování Boltona Rusko ponechalo bez komentáře. „Není to otázka na nás, ale na americkou administrativu,“ řekl kremelský mluvčí Dmitrij Peskov. Podle Reuters zároveň dodal, že Rusko doufá, že v Bílém domě bude více lidí, kteří jsou schopni hledět za nynější vlnu rusofobie. Někteří komentátoři označují příchod Boltona za další hřebík do rakve pro mezinárodní jadernou dohodu s Íránem. Dokument mezi Teheránem a světovými mocnostmi, který Trump tvrdě kritizuje, vznikl v éře bývalého demokratického prezidenta Baracka Obamy.";

    var words = source_text.split(" ");
    //var words = shuffle(split);
    //var classMaster = Math.floor(Math.random() * words.length());
    for (var i = 0; i < words.length - 1; i++) {

        $('#Demo').append('<span class="word word_' + i + '">' + (words[i] += " ") +'</span>').show();

    }
    console.log(words.length);
    console.log(words);

    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    setInterval(function(){

        var element1 = Math.floor(Math.random() * (words.length-2));
        var element2 = Math.floor(Math.random() * (words.length-2));
        var element3 = Math.floor(Math.random() * (words.length-2));
        var element4 = Math.floor(Math.random() * (words.length-2));
        var element5 = Math.floor(Math.random() * (words.length-2));

        var span_1 = $("span.word_" + element1);
        var span_2 = $("span.word_" + element2);
        var span_3 = $("span.word_" + element3);
        var span_4 = $("span.word_" + element4);
        var span_5 = $("span.word_" + element5);

        var size_1 = Math.floor(Math.random() * 50);
        var size_2 = Math.floor(Math.random() * 60) + 10;
        var size_3 = Math.floor(Math.random() * 70) + 20;
        var size_4 = Math.floor(Math.random() * 80) + 30;
        var size_5 = Math.floor(Math.random() * 900) + 400;

        span_1.animate({fontSize: size_1}, "slow");
        span_2.animate({fontSize: size_2}, "slow");
        span_3.animate({fontSize: size_3}, "slow");
        span_4.animate({fontSize: size_4}, "slow");
        span_5.animate({fontSize: size_5}, "slow");

        span_1.css({ "color" : "red" });
        span_2.css({ "color" : "black" });
        span_3.css({ "font-weight" : "bold" });
        span_4.css({ "text-decoration" : "line-through" });
        span_5.css({ "text-decoration" : "none" });

    }, 4000);

    setInterval(function(){

        var element1 = Math.floor(Math.random() * (words.length-2));
        var element2 = Math.floor(Math.random() * (words.length-2));

        var selector1 = $("span.word_" + element1);
        var selector2 = $("span.word_" + element2);

        var text1 = $(selector2).text();
        var text2 = $(selector1).text();
        console.log(text1);
        console.log(text2);

        $(selector1).fadeOut(750, function() {
            $(this).text(text1).fadeIn(750);
        });

        $(selector2).fadeOut(750, function() {
            $(this).text(text2).fadeIn(750);
        });


    }, 400);

    setInterval(function(){

        var rotationField = ["vertical_right", "vertical_left", "horizontal_reverse"];

        var element1 = Math.floor(Math.random() * (words.length-2));
        var element2 = Math.floor(Math.random() * (words.length-2));
        var selector1 = $("span.word_" + element1);
        var selector2 = $("span.word_" + element2);

        for(i=0; i < rotationField.length; i++){
            if ( $(selector1).hasClass(rotationField[i]) ) {

                $(selector1).removeClass("vertical_right vertical_left horizontal_reverse");

            }else {

                var randomClass = rotationField[Math.floor(Math.random()*rotationField.length)];
                console.log("------------------------------" + randomClass);
                $(selector1, selector2).addClass(randomClass);

            }

        }

    }, 1500);







});