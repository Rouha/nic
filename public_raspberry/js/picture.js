$(document).ready(function() {

    // DOM PREPARE

    var windowWidth = $(document).width();
    var windowHeight = $(document).height();

    var startBox = $("#StartBox");

    $(startBox).css("width", windowWidth / 4);
    $(startBox).css("height", windowHeight / 2);
    $(startBox).css( { marginLeft : "auto", marginRight : "auto", marginBottom : "0px", marginTop: windowHeight / 10 } );

    // CONTENT PREPARE

    var source_text = "Nový bezpečnostní poradce amerického prezidenta Donalda Trumpa John Bolton, který v dubnu nahradí odvolaného Herberta Raymonda McMastera, vyvolává především na Blízkém východě podle agentury Reuters rozporuplné reakce. Bolton je znám ostrými názory a konfrontačními postoji vůči Íránu, KLDR i Rusku, naopak je obhájcem politiky Izraele. Devětašedesátiletý Bolton v minulosti prosazoval použití vojenské síly proti Íránu či Severní Koreji a velmi ostře se stavěl například i k Rusku. V prvních dvou případech volí i Trump konfrontační politiku, k Rusku má však méně kritický přístup. Nynější Trumpovo rozhodnutí pogratulovat ruskému prezidentovi Vladimiru Putinovi ke znovuzvolení, ale označil za zdvořilostní záležitost. Trumpovi poradci přitom prezidentovi doporučili, aby Putinovi neblahopřál. Jmenování Boltona Rusko ponechalo bez komentáře. „Není to otázka na nás, ale na americkou administrativu,“ řekl kremelský mluvčí Dmitrij Peskov. Podle Reuters zároveň dodal, že Rusko doufá, že v Bílém domě bude více lidí, kteří jsou schopni hledět za nynější vlnu rusofobie. Někteří komentátoři označují příchod Boltona za další hřebík do rakve pro mezinárodní jadernou dohodu s Íránem. Dokument mezi Teheránem a světovými mocnostmi, který Trump tvrdě kritizuje, vznikl v éře bývalého demokratického prezidenta Baracka Obamy.";

    var words = source_text.split(" ");
    var wordsInfo = [];

    for (var i = 0; i < words.length; i++) {

        $(startBox).append('<span class="startPhase word word_' + i + '">' + (words[i] += " ") +'</span>').show();

        var span = $(".word_" + i);
        var offset = $(span).offset();

        var range = Math.floor(Math.random() * 50);
        var range2 = Math.floor(Math.random() * 49);

        wordsInfo.push({
            number: i,
            startTop: offset.top,
            startLeft:  offset.left,
            currentTop: offset.top,
            currentLeft: offset.left,
            angle: 0,
            fontSize: 16,
            color: "rgb(0, 0, 0)",
            start: range,
            end: range + range2,
            maxWidth: windowWidth, //todle je asi zbytečné - máme window paramatry po načtení aplikace a to stačí
            maxHeight: windowHeight
        });

    }

    console.log(wordsInfo);

    absoluteMassacre();

    function absoluteMassacre(){

        for (var i = 0; i < wordsInfo.length; i++) {

            $(".word_" + wordsInfo[i].number).css({ top: wordsInfo[i].startTop, left: wordsInfo[i].startLeft, position: "absolute" }).removeClass("startPhase");

        }

    }

    //$("#Slider").on('input', function() { $("#DistanceValue").html(this.value); });

    //MAIN INTERVAL
    var socket = io();
	
	var floatingFilterArray = [];
	
	var filteredDistance = 100;
	
	socket.on('distance', function(data) { //get light switch status from client
        if (data) {
		    data = Math.round(data);

			if (Math.round(data) < 100 && Math.round(data) != 0){

                floatingFilterArray.push(Math.round(data));

            }else {
                floatingFilterArray.push(100);

            }

			if (floatingFilterArray.length == 50) {
				var floatingFilterArrayCopy = Array.from(floatingFilterArray).sort(); // pracovni kopie pole
				
				filteredDistance = floatingFilterArrayCopy[25];

                floatingFilterArray.shift();
			}
		}

    });
	
    var lastDistanceValue = $("#DistanceValue").text();

    var pictureBody = $("#PictureBody");

    setInterval(function () {

        document.getElementById("DistanceValue").innerHTML = filteredDistaynce;

        var currentDistance = filteredDistance;
        var diffenceDistance = lastDistanceValue - currentDistance;
        lastDistanceValue = currentDistance;

        for (var i = 0; i < wordsInfo.length; i++){

            var exactWord = $(".word_" + i);

            if ((wordsInfo[i].start < currentDistance)){

                if ((wordsInfo[i].end > currentDistance)) {

                    $(exactWord).addClass("beingUsed");

                    // ACTUAL VALUES

                    wordsInfo[i].currentLeft += diffenceDistance;
                    wordsInfo[i].currentTop += diffenceDistance;
                    wordsInfo[i].angle += diffenceDistance;
                    wordsInfo[i].fontSize += diffenceDistance;
                    //wordsInfo[i].color = "rgb("

                    $(pictureBody).addClass("notDefaultState");
                    //console.log(wordsInfo[i].start + " " + currentDistance + "  " + wordsInfo[i].end);
                    $(exactWord).removeClass("rotateCheat");
                    var angle = "rotate(" + (wordsInfo[i].angle) + "deg)";
                    //console.log(angle);
                    $(exactWord).css({
                        "font-size" : wordsInfo[i].fontSize,
                        "transform": angle,
                        "color" : "rgb(" + currentDistance * 4 + ", 0, 0)",
                        "top" : wordsInfo[i].currentTop + "px",
                        "left" : wordsInfo[i].currentLeft + "px"
                    });

                }
                else {

                    $(exactWord).removeClass("beingUsed");

                }

            }

        }

    }, 34);

    // BACK TO START PHRASE

    setInterval(function () {

        if (filteredDistance >= 100 && $(pictureBody).hasClass("notDefaultState")){

            $(pictureBody).removeClass("notDefaultState");
            startingPhase();

        }

    }, 1000);

    function startingPhase() {

        for (var i = 0; i < wordsInfo.length; i++){

            var startingWord = $(".word_" + i);

            $(startingWord).addClass("rotateCheat");
            $(startingWord).animate({
                left: wordsInfo[i].startLeft,
                top: wordsInfo[i].startTop,
                "font-size": "16px",
                "color": "black"

            });

            wordsInfo[i].currentLeft = wordsInfo[i].startLeft;
            wordsInfo[i].currentTop = wordsInfo[i].startTop;
            wordsInfo[i].angle = 0;
            wordsInfo[i].fontSize = 16;
            wordsInfo[i].color = "rgb(0, 0, 0)";


        }

        console.log("RESET");

    }


});
