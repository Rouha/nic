<!DOCTYPE html>
<html>
<head>
    <title>Jan Rouha | Grombel</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="styles/dist/compiled_styles.min.css">
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script type = "text/javascript" src = "js/jquery213.min.js"></script>

    <script type = "text/javascript" src = "js/jquery-ui1113.min.js"></script>

    <script type = "text/javascript" src = "js/main3.js"></script>


    <link rel="shortcut icon" type="image/png" href=""/>

</head>
<body>

<div id="Text">

    <span class="word_1" style="top: 0; left: 0; position: relative;">Jedna</span>

    <span class="word_2" style="top: 0; left: 0; position: relative;">dva</span>

    <span class="word_3" style="top: 0; left: 0; position: relative;">tři</span>

    <span class="word_4" style="top: 0; left: 0; position: relative;">čtyři</span>

    <span class="word_5" style="top: 0; left: 0; position: relative;">pět.</span>

</div>

</body>
</html>